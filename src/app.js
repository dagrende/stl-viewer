import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import { STLLoader } from 'three/examples/jsm/loaders/STLLoader.js';


// const OrbitControls = require('./three-orbit-controls')(THREE);

const scene = new THREE.Scene();
scene.rotateX(-Math.PI / 2);
scene.rotateY(-Math.PI);
scene.rotateX(-Math.PI);

let camera = new THREE.PerspectiveCamera(50, 1, 0.1, 1000);
camera.position.set(2, 2, -5);

let renderer = new THREE.WebGLRenderer({antialias: true});
renderer.setSize( window.innerWidth, window.innerHeight );
renderer.setClearColor(0xdddddd);

const controls = new OrbitControls(camera, document.body);
controls.enablePan = false;

document.body.appendChild( renderer.domElement );

const ambientLight = new THREE.AmbientLight( 0x555555 );
scene.add( ambientLight );

const light = new THREE.PointLight(0xffffff);
light.position.set(0, -5, 0);
scene.add(light);

scene.add(new THREE.AxesHelper( 500 ));

const material = new THREE.MeshPhongMaterial({ color: 0xdddddd, specular: 0x080808, shininess: 60 });
// THREE.FlatShading = true;




// var geometry = new THREE.BoxGeometry();
// var cube = new THREE.Mesh( geometry, material );
// scene.add( cube );



let loader = new STLLoader();
loader.load( 'http://localhost:31338/fan-blade.stl', function ( geometry ) {
   console.log('loaded stl');
   const newMesh = new THREE.Mesh(geometry, material);
   newMesh.geometry.computeVertexNormals();
   scene.add(newMesh);

   ScaleIntoView(geometry);

   // render model initially and on camera movement
   function render() {
     light.position.set(-camera.position.x, camera.position.z, camera.position.y + 1);
     renderer.render(scene, camera);
     console.log('.');
   }
   controls.addEventListener('change', render);
   render();

   function ScaleIntoView(geom) {
     geom.computeBoundingSphere();
     let bs = geom.boundingSphere;
     let pos = camera.position;
     let dist = pos.distanceTo(bs.center);
     let d = dist * Math.sin(50/2 / 180 * Math.PI);
     let factor = bs.radius * 3 + bs.center.length();
     let cp = camera.position;
     let cplen = cp.length();
     cp.set(factor * cp.x / cplen, factor * cp.y / cplen, factor * cp.z / cplen);
     camera.updateProjectionMatrix();
   }
 }, console.log, console.error)
